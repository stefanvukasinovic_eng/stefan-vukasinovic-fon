# Student service

Repository for student service web application.

### Technologies
* Java 8
* Spring MVC
* JSP
* JPA
* Hibernate
* MySQL