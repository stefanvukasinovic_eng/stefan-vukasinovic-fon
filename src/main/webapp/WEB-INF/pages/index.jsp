<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Student service</title>
	<link rel='stylesheet' href='webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
	<link rel='stylesheet' href='webjars/font-awesome/5.6.3/css/all.min.css'>
	<link href='resources/css/styles.css' rel="stylesheet" />
</head>
<body>
	<%@include file="navbar.jsp"%>
	<div class="jumbotron jumbotron-fluid mb-3 text-center">
		<div class="container">
			<h1 class="display-3">Welcome to Student service!</h1>
	    	<p class="lead">This is a service for keeping track of data about all students, professors and their subjects.</p>
	  	</div>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center my-4">
		    <div class="col-3 justify-content-center">
		    	<div class="tile green d-flex align-items-center">
			    	<a href="students">Students</a>
			    </div>
		    </div>
		    <div class="col-3 justify-content-center">
		    	<div class="tile green d-flex align-items-center">
			    	<a href="professors">Professors</a>
			    </div>
		    </div>
		    <div class="col-3 justify-content-center">
		    	<div class="tile green d-flex align-items-center">
		    		<a href="subjects">Subjects</a>
			    </div>
		    </div>
		</div>
		<div class="row justify-content-center my-4">
		    <div class="col-3 justify-content-center">
			    <div class="tile orange d-flex align-items-center">
			    	<a href="exams">Exams</a>			    
			    </div>
		    </div>
		    <div class="col-3 justify-content-center">
			    <div class="tile orange d-flex align-items-center">
			    	<a href="exam_registrations">Exam registrations</a>
			    </div>
		    </div>
		</div>
	</div>
</body>
<script type="text/javascript" src="webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</html>