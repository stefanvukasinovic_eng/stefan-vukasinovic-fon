<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Student Service - Exam registration list</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap-table/1.16.0/dist/bootstrap-table.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/font-awesome/5.6.3/css/all.min.css'>
	<link href='${pageContext.request.contextPath}/resources/css/styles.css' rel="stylesheet"/>
</head>
<body>
	<%@include file="../navbar.jsp"%>
	<%@include file="../alerts.jsp"%>
	<div class="jumbotron jumbotron-fluid mb-3 text-center">
		<div class="container">
			<h1 class="display-4">List of exam registrations</h1>
	  	</div>
	</div>
	<div class="container-fluid">
		<a type="button" class="btn btn-success ml-4 mb-3" href="/Stefan-Vukasinovic-FON/exam_registrations/register">
		  <i class="fas fa-plus mr-2"></i>Register an exam
		</a>
		<table id="table" 
			  class="table text-center"
			  data-toggle="table"
			  data-pagination="true"
			  data-striped="true"
			  data-page-size="10"
			  data-page-list="[5, 10, 25, 50, 100]"
			  data-url="/Stefan-Vukasinovic-FON/rest/exam_registrations"
			  data-side-pagination="server"
			  data-row-style="rowStyle">
			<thead>
			    <tr>
					<th data-formatter="indexFormatter">#</th>
					<th data-formatter="examDateFormatter">Date</th>
					<th data-field="exam.subject.name">Subject</th>
					<th data-formatter="professorFormatter">Professor</th>
					<th data-field="student.indexNumber">Student</th>
					<th data-formatter="dateFormatter">Date of registration</th>
			    </tr>
		  	</thead>
		</table>
	</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/popper.js/1.16.0/umd/popper.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap-table/1.16.0/dist/bootstrap-table.min.js"></script>
<script>

  function examDateFormatter(value, row, index) {
	  const date = new Date();
	  date.setUTCDate(row['exam'].date.dayOfMonth);
	  date.setUTCMonth(row['exam'].date.monthValue - 1);
  	  date.setUTCFullYear(row['exam'].date.year);
	  return date.toLocaleDateString();
  }

  function dateFormatter(value, row, index) {
	  const date = new Date();
	  date.setUTCDate(row['date'].dayOfMonth);
	  date.setUTCMonth(row['date'].monthValue - 1);
	  date.setUTCFullYear(row['date'].year);
	  return date.toLocaleDateString();
  }

  function indexFormatter(value, row, index) {
	  return index + 1;
  }
  
  function professorFormatter(value, row, index) {
	  return row['exam'].professor.firstName + ' ' + row['exam'].professor.lastName;
  }
</script>
</body>
</html>