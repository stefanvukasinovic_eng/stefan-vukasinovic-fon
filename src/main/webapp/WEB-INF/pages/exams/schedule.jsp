<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Student Service - Schedule an exam</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap-table/1.16.0/dist/bootstrap-table.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/font-awesome/5.6.3/css/all.min.css'>
	<link href='${pageContext.request.contextPath}/resources/css/styles.css' rel="stylesheet"/>
</head>
<body>
	<%@include file="../navbar.jsp"%>
	<%@include file="../alerts.jsp"%>
	<div id="overlayDiv"></div>
	<div class="jumbotron jumbotron-fluid mb-3 text-center">
		<div class="container">
			<h1 class="display-4">Schedule an exam</h1>
	  	</div>
	</div>
	<div class="container-fluid d-flex justify-content-center">
		<form:form id="form" action="/Stefan-Vukasinovic-FON/exams" method="post" modelAttribute="examDto" class="w-75 mb-4">
          	<c:if test="${not empty invalid}">
			   <div class="alert alert-danger" role="alert mb-2">${invalid}</div>
			</c:if>
			<div class="form-label-group mt-3 mb-4">
				<label for="date">Date</label>
				<form:input type="date" path="date" id="date" class="form-control" placeholder="Date" />
				<div class="text-danger">
					<form:errors path="date" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="subjectId">Subject</label>
				<form:select path="subjectId" id="subjectId" class="form-control">
				    <form:options items="${subjects}" />
				</form:select>
				<div class="text-danger">
					<form:errors path="subjectId" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="professorId">Professor</label>
				<form:select path="professorId" id="professorId" class="form-control">
				    <form:options items="${professors}" />
				</form:select>
				<div class="text-danger">
					<form:errors path="professorId" cssClass="error" />
				</div>
			</div>			
			<button type="submit" class="btn btn-primary">Schedule</button>
       		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	      </form:form>
	</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
	const successAlertMessage = '${successAlertMessage}';
	if (successAlertMessage) {
		  $("#overlayDiv").addClass("overlay");
		  $("#successAlertMessage").text(successAlertMessage);
		  $("#successAlert").fadeTo(2500, 800).slideUp(800, function(){
		      $("#successAlert").slideUp(800);
		      window.location.replace("/Stefan-Vukasinovic-FON/exams");
		  });
		  $('#successAlert').on('close.bs.alert', function () {
			  window.location.replace("/Stefan-Vukasinovic-FON/exams");
		  });
	}
</script>
</body>
</html>