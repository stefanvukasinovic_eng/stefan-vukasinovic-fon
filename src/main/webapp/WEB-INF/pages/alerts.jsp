<div id="successAlert" class="alert alert-success alert-dismissible fixed-top autoCloseAlert collapse" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h5 class="alert-heading">Success!</h5>
    <p id="successAlertMessage"></p>
</div>

<div id="failAlert" class="alert alert-danger alert-dismissible fixed-top autoCloseAlert collapse" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h5 class="alert-heading">Error!</h5>
    <p id="failAlertMessage"></p>
</div>	