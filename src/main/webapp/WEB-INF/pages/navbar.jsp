<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="${pageContext.request.contextPath}">Student Service</a>
    <ul class="navbar-nav mr-auto d-flex justify-content-end w-100">
    	<security:authorize access="hasRole('ROLE_TELLER')">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">				
					<security:authentication property="principal.username" />
				</a>
				<div class="dropdown-menu">
				    <a class="dropdown-item" href="authentication/logout"><i class="fas fa-sign-out-alt mr-2"></i>Logout</a>
				</div>
			</li>
		</security:authorize>
      	<security:authorize access="!isAuthenticated()">
      		<li class="nav-item">
			  <a class="btn btn-success" role="button" href="${pageContext.request.contextPath}/authentication/login"><i class="fas fa-sign-in-alt mr-2"></i>Login</a>
			</li>
		</security:authorize>
   	</ul>
</nav>