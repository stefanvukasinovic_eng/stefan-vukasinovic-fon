<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Student Service - Add or update subject</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap-table/1.16.0/dist/bootstrap-table.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/font-awesome/5.6.3/css/all.min.css'>
	<link href='${pageContext.request.contextPath}/resources/css/styles.css' rel="stylesheet"/>
</head>
<body>
	<%@include file="../navbar.jsp"%>
	<%@include file="../alerts.jsp"%>
	<div id="overlayDiv"></div>
	<div class="jumbotron jumbotron-fluid mb-3 text-center">
		<div class="container">
			<c:choose>
			<c:when test="${subject['id'] != null}">
				<h1 class="display-4">Update subject</h1>
			</c:when>
			<c:otherwise>
				<h1 class="display-4">Add subject</h1>
			</c:otherwise>
		</c:choose>
	  	</div>
	</div>
	<div class="container-fluid d-flex justify-content-center">
		<form:form id="form" action="/Stefan-Vukasinovic-FON/subjects" method="post" modelAttribute="subject" class="w-75 mb-4">
          	<c:if test="${not empty invalid}">
			   <div class="alert alert-danger" role="alert mb-2">${invalid}</div>
			</c:if>
			<form:input type="text" path="id" id="id" hidden="hidden" />
			<div class="form-label-group mt-3 mb-4">
				<label for="name">Name</label>
				<form:input type="text" path="name" id="name" class="form-control" placeholder="Name" autofocus="autofocus" />
				<div class="text-danger">
					<form:errors path="name" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="description">Description</label>
				<form:textarea path="description" id="description" class="form-control" placeholder="Description" rows="5" />
				<div class="text-danger">
					<form:errors path="description" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="yearOfStudy">Year of study</label>
				<form:input type="number" path="yearOfStudy" id="yearOfStudy" class="form-control" placeholder="Year of study" />
				<div class="text-danger">
					<form:errors path="yearOfStudy" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="semester">Semester</label>
				<form:select path="semester" id="semester" class="form-control">
				    <form:options items="${semesters}" />
				</form:select>
			</div>		
			<c:choose>
				<c:when test="${subject['id'] != null}">
			        <button type="submit" class="btn btn-primary">Update</button>
				</c:when>
				<c:otherwise>
					<button type="submit" class="btn btn-primary">Add</button>
				</c:otherwise>
			</c:choose>
       		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	      </form:form>
	</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
	const successAlertMessage = '${successAlertMessage}';
	if (successAlertMessage) {
		  $("#overlayDiv").addClass("overlay");
		  $("#successAlertMessage").text(successAlertMessage);
		  $("#successAlert").fadeTo(2500, 800).slideUp(800, function(){
		      $("#successAlert").slideUp(800);
		      window.location.replace("/Stefan-Vukasinovic-FON/subjects");
		  });
		  $('#successAlert').on('close.bs.alert', function () {
			  window.location.replace("/Stefan-Vukasinovic-FON/subjects");
		  });
	}
</script>
</body>
</html>