<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Student Service - Professor list</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap-table/1.16.0/dist/bootstrap-table.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/font-awesome/5.6.3/css/all.min.css'>
	<link href='${pageContext.request.contextPath}/resources/css/styles.css' rel="stylesheet"/>
</head>
<body>
	<%@include file="../navbar.jsp"%>
	<%@include file="../alerts.jsp"%>
	<%@include file="../delete-modal.html"%>
	<div class="jumbotron jumbotron-fluid mb-3 text-center">
		<div class="container">
			<h1 class="display-4">List of professors</h1>
	  	</div>
	</div>
	<div class="container-fluid">
		<a type="button" class="btn btn-success ml-4 mb-3" href="/Stefan-Vukasinovic-FON/professors/add">
		  <i class="fas fa-plus mr-2"></i>Add professor
		</a>
		<table id="table" 
			  class="table text-center"
			  data-toggle="table"
			  data-pagination="true"
			  data-striped="true"
			  data-page-size="10"
			  data-page-list="[5, 10, 25, 50, 100]"
			  data-url="/Stefan-Vukasinovic-FON/rest/professors"
			  data-side-pagination="server"
			  data-detail-view="true"
			  data-detail-view-by-click="true"
			  data-detail-formatter="detailFormatter"
			  data-row-style="rowStyle">
			<thead>
			    <tr>
					<th data-formatter="indexFormatter">#</th>
					<th data-field="firstName">First name</th>
					<th data-field="lastName">Last name</th>
					<th data-field="title.name">Title</th>
			    </tr>
		  	</thead>
		</table>
	</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/popper.js/1.16.0/umd/popper.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap-table/1.16.0/dist/bootstrap-table.min.js"></script>
<script>
  function detailFormatter(index, row) {
    let html = [];
    
    const email = row['email'] ? row['email'] : '/';
    const cityName = row['city'] ? row['city'].name : '/';
    const cityPostalCode = row['city'] ? row['city'].postalCode : '/';
    const address = row['address'] ? row['address'] : '/';
    const phone = row['phone'] ? row['phone'] : '/';
    
    const reelectionDate = new Date();
    reelectionDate.setUTCDate(row['reelectionDate'].dayOfMonth);
    reelectionDate.setUTCMonth(row['reelectionDate'].monthValue - 1);
    reelectionDate.setUTCFullYear(row['reelectionDate'].year);
    
    html.push('<div class="card w-75" style="margin: auto;">');
  	html.push('	<div class="card-body">');
	html.push('		<h2 class="card-title">' + row['firstName'] + ' ' + row['lastName'] + '</h2>');
	html.push('		<div class="row justify-content-center">');
	html.push('			<div class="col-4">');
	html.push('     		<p class="card-text"><b>Email</b>: ' + email + '</p>');
	html.push('     	</div>');
	html.push('			<div class="col-4">');
	html.push('     		<p class="card-text"><b>Reelection date</b>: ' + reelectionDate.toLocaleDateString() + '</p>');
	html.push('     	</div>');
	html.push('     </div>');
	html.push('		<div class="row justify-content-center">');
	html.push('			<div class="col-4">');
	html.push('     		<p class="card-text"><b>City</b>: ' + cityName + '</p>');
	html.push('     	</div>');
	html.push('			<div class="col-4">');
	html.push('     		<p class="card-text"><b>Postal Code</b>: ' + cityPostalCode + '</p>');
	html.push('     	</div>');
	html.push('     </div>');
	html.push('		<div class="row justify-content-center">');
	html.push('			<div class="col-4">');
	html.push('     		<p class="card-text"><b>Address</b>: ' + address + '</p>');
	html.push('     	</div>');
	html.push('			<div class="col-4">');
	html.push('     		<p class="card-text"><b>Phone</b>: ' + phone + '</p>');
	html.push('     	</div>');
	html.push('     </div>');
    html.push('		<a class="btn btn-warning mx-2 my-3" href="/Stefan-Vukasinovic-FON/professors/' + row['id'] + '/update"><i class="fas fa-user-edit mr-2"></i>Edit</a>');
 	html.push('		<button class="btn btn-danger mx-2 my-3" data-toggle="modal" data-target="#deleteModal" onclick="rememberId(' + row['id'] + ')"><i class="fas fa-user-minus mr-2"></i>Delete</button>');
 	html.push('	</div>');
 	html.push('</div>');
    return html.join('');
  }
  
  function rowStyle(row, index) {
      return {
         classes: 'table-row'
      }
  }
  
  function indexFormatter(value, row, index) {
	  return index + 1;
  }
  
  let deleteId;
  
  function rememberId(id) {
	  deleteId = id;
  }
  
  function deleteEntity() {
  	  $.ajax({
		    url: '/Stefan-Vukasinovic-FON/rest/professors/' + deleteId,
		    type: 'delete',
		    success: function(result) {
		    	$('#table').bootstrapTable('refresh');
		    	$("#successAlertMessage").text(result.message);
		    	$("#successAlert").fadeTo(3500, 800).slideUp(800, function(){
		    	    $("#successAlert").slideUp(800);
		    	});
		    },
		    error: function(errorResult) {
		    	const message = errorResult && errorResult.responseText ? errorResult.responseText : "Something went wrong. Please try again later."
		    	$("#failAlertMessage").text(message);
		    	$("#failAlert").fadeTo(3500, 800).slideUp(800, function(){
		    	    $("#failAlert").slideUp(800);
		    	});
		    }
	  });
  }
</script>
</body>
</html>