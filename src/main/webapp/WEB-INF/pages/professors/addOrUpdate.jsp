<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>Student Service - Add or update professor</title>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/bootstrap-table/1.16.0/dist/bootstrap-table.min.css'>
	<link rel='stylesheet' href='${pageContext.request.contextPath}/webjars/font-awesome/5.6.3/css/all.min.css'>
	<link href='${pageContext.request.contextPath}/resources/css/styles.css' rel="stylesheet"/>
</head>
<body>
	<%@include file="../navbar.jsp"%>
	<%@include file="../alerts.jsp"%>
	<div id="overlayDiv"></div>
	<div class="jumbotron jumbotron-fluid mb-3 text-center">
		<div class="container">
			<c:choose>
			<c:when test="${professorDto['id'] != null}">
				<h1 class="display-4">Update professor</h1>
			</c:when>
			<c:otherwise>
				<h1 class="display-4">Add professor</h1>
			</c:otherwise>
		</c:choose>
	  	</div>
	</div>
	<div class="container-fluid d-flex justify-content-center">
		<form:form id="form" action="/Stefan-Vukasinovic-FON/professors" method="post" modelAttribute="professorDto" class="w-75 mb-4">
          	<c:if test="${not empty invalid}">
			   <div class="alert alert-danger" role="alert mb-2">${invalid}</div>
			</c:if>
			<form:input type="text" path="id" id="id" hidden="hidden" />
			<div class="form-label-group mt-3 mb-4">
				<label for="firstName">First name</label>
				<form:input type="text" path="firstName" id="firstName" class="form-control"
					 placeholder="First name" autofocus="autofocus" />
				<div class="text-danger">
					<form:errors path="firstName" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="lastName">Last name</label>
				<form:input type="text" path="lastName" id="lastName" class="form-control"
					 placeholder="Last name" />
				<div class="text-danger">
					<form:errors path="lastName" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="email">Email</label>
				<form:input type="email" path="email" id="email" class="form-control" placeholder="Email"  />
				<div class="text-danger">
					<form:errors path="email" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="address">Address</label>
				<form:input type="text" path="address" id="address" class="form-control" placeholder="Address" />
				<div class="text-danger">
					<form:errors path="address" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="cityId">City</label>
				<form:select path="cityId" id="cityId" class="form-control">
				    <form:options items="${cities}" />
				</form:select>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="phone">Phone</label>
				<form:input type="text" path="phone" id="phone" class="form-control" placeholder="Phone" />
				<div class="text-danger">
					<form:errors path="phone" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="reelectionDate">Reelection date</label>
				<form:input type="date" path="reelectionDate" id="reelectionDate" class="form-control" placeholder="Reelection date" />
				<div class="text-danger">
					<form:errors path="reelectionDate" cssClass="error" />
				</div>
			</div>
			<div class="form-label-group mt-3 mb-4">
				<label for="titleId">Title</label>
				<form:select path="titleId" id="titleId" class="form-control">
				    <form:options items="${titles}" />
				</form:select>
				<div class="text-danger">
					<form:errors path="titleId" cssClass="error" />
				</div>
			</div>		
			<c:choose>
				<c:when test="${professorDto['id'] != null}">
			        <button type="submit" class="btn btn-primary">Update</button>
				</c:when>
				<c:otherwise>
					<button type="submit" class="btn btn-primary">Add</button>
				</c:otherwise>
			</c:choose>
       		<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	      </form:form>
	</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
	const successAlertMessage = '${successAlertMessage}';
	if (successAlertMessage) {
		  $("#overlayDiv").addClass("overlay");
		  $("#successAlertMessage").text(successAlertMessage);
		  $("#successAlert").fadeTo(2500, 800).slideUp(800, function(){
		      $("#successAlert").slideUp(800);
		      window.location.replace("/Stefan-Vukasinovic-FON/professors");
		  });
		  $('#successAlert').on('close.bs.alert', function () {
			  window.location.replace("/Stefan-Vukasinovic-FON/professors");
		  });
	}
</script>
</body>
</html>