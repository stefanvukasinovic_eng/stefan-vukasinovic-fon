package stefan.vukasinovic.fon.studentservice.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import stefan.vukasinovic.fon.studentservice.entity.Subject;
import stefan.vukasinovic.fon.studentservice.model.EntityListDto;
import stefan.vukasinovic.fon.studentservice.model.ResponseDto;
import stefan.vukasinovic.fon.studentservice.service.SubjectService;

@RestController
@RequestMapping("/rest/subjects")
public class SubjectRestController {

	private SubjectService subjectService;
	
	@Autowired
	public SubjectRestController(SubjectService subjectService) {
		this.subjectService = subjectService;
	}
	
	@GetMapping
	public ResponseEntity<EntityListDto<Subject>> getSubjects(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
		int page = (offset / limit);
		System.out.println("Retrieving subjects page " + page + " with size " + limit);
		List<Subject> subjects = this.subjectService.findAll(page, limit).getContent();
		System.out.println("Number of subjects retrieved: " + subjects.size());
		return ResponseEntity.ok(new EntityListDto<Subject>(this.subjectService.count(), subjects));
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		System.out.println("Deleting subject with id " + id);
		try {
			this.subjectService.delete(id);
			return ResponseEntity.ok(new ResponseDto("You have deleted subject with id " + id + " successfully", true));
		} catch(Exception e) {
			return ResponseEntity.badRequest().body("Subject with id " + id + " is not deleted successfully");
		}
	}
}