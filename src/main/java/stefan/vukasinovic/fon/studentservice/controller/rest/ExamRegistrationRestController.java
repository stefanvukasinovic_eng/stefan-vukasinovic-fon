package stefan.vukasinovic.fon.studentservice.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import stefan.vukasinovic.fon.studentservice.entity.ExamRegistration;
import stefan.vukasinovic.fon.studentservice.model.EntityListDto;
import stefan.vukasinovic.fon.studentservice.service.ExamRegistrationService;

@RestController
@RequestMapping("/rest/exam_registrations")
public class ExamRegistrationRestController {

	private ExamRegistrationService examRegistrationService;
	
	@Autowired
	public ExamRegistrationRestController(ExamRegistrationService examRegistrationService) {
		this.examRegistrationService = examRegistrationService;
	}
	
	@GetMapping
	public ResponseEntity<EntityListDto<ExamRegistration>> getExamRegistrations(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
		int page = (offset / limit);
		System.out.println("Retrieving exam registrations page " + page + " with size " + limit);
		List<ExamRegistration> examRegistrations = this.examRegistrationService.findAll(page, limit).getContent();
		System.out.println("Number of exam registrations retrieved: " + examRegistrations.size());
		return ResponseEntity.ok(new EntityListDto<ExamRegistration>(this.examRegistrationService.count(), examRegistrations));
	}
}