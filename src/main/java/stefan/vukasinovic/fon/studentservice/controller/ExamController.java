package stefan.vukasinovic.fon.studentservice.controller;

import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.entity.Professor;
import stefan.vukasinovic.fon.studentservice.entity.Subject;
import stefan.vukasinovic.fon.studentservice.model.ExamDto;
import stefan.vukasinovic.fon.studentservice.service.ExamService;
import stefan.vukasinovic.fon.studentservice.service.ProfessorService;
import stefan.vukasinovic.fon.studentservice.service.SubjectService;
import stefan.vukasinovic.fon.studentservice.validator.ExamValidator;

@Controller
@RequestMapping(value = "/exams")
public class ExamController {

	private ExamService examService;
	private SubjectService subjectService;
	private ProfessorService professorService;
	private ExamValidator examValidator;
	
	@Autowired
	public ExamController(ExamService examService, SubjectService subjectService, ProfessorService professorService,
			ExamValidator examValidator) {
		this.examService = examService;
		this.subjectService = subjectService;
		this.professorService = professorService;
		this.examValidator = examValidator;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(examValidator);
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@GetMapping
	public ModelAndView getAllExams(Model model) {
		return new ModelAndView("exams/list");
	}
	
	@GetMapping(value = "/schedule")
	public ModelAndView addExam(Model model) {
		return new ModelAndView("exams/schedule");
	}
	
	@PostMapping
	public String add(@ModelAttribute @Validated ExamDto examDto, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("invalid", "One or more fields are invalid");
			return "exams/schedule";
	    }
		
		Exam exam = new Exam();
		exam.setDate(LocalDate.parse(examDto.getDate()));
		
		Subject subject = this.subjectService.findById(examDto.getSubjectId());
		exam.setSubject(subject);
		
		Professor professor = this.professorService.findById(examDto.getProfessorId());
		exam.setProfessor(professor);
		
		try {
			this.examService.save(exam);
			model.addAttribute("successAlertMessage", "Exam is scheduled successfully");
			return "exams/schedule";
		} catch (Exception e) {
			model.addAttribute("invalid", "Scheduling of exam has failed, try again later");
			return "exams/schedule";
		}
	}
	
	@ModelAttribute(name = "examDto")
	private ExamDto createExamDto() {
		return new ExamDto();
	}
	
	@ModelAttribute(name = "subjects")
	private Map<Long, String> getSubjects() {
		return this.subjectService
				.findAll()
				.stream()
				.collect(Collectors.toMap(Subject::getId, subject -> subject.getName()));
	}
	
	@ModelAttribute(name = "professors")
	private Map<Long, String> getProfessors() {
		return this.professorService
				.findAll()
				.stream()
				.collect(Collectors.toMap(Professor::getId, professor -> professor.getFirstName() + " " + professor.getLastName()));
	}
}
