package stefan.vukasinovic.fon.studentservice.controller;

import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.entity.ExamRegistration;
import stefan.vukasinovic.fon.studentservice.entity.Student;
import stefan.vukasinovic.fon.studentservice.model.ExamRegistrationDto;
import stefan.vukasinovic.fon.studentservice.service.ExamRegistrationService;
import stefan.vukasinovic.fon.studentservice.service.ExamService;
import stefan.vukasinovic.fon.studentservice.service.StudentService;
import stefan.vukasinovic.fon.studentservice.validator.ExamRegistrationValidator;

@Controller
@RequestMapping(value = "/exam_registrations")
public class ExamRegistrationController {

	private ExamRegistrationService examRegistrationService;
	private ExamService examService;
	private StudentService studentService;
	private ExamRegistrationValidator examRegistrationValidator;

	@Autowired
	public ExamRegistrationController(ExamRegistrationService examRegistrationService, ExamService examService,
			StudentService studentService, ExamRegistrationValidator examRegistrationValidator) {
		this.examRegistrationService = examRegistrationService;
		this.examService = examService;
		this.studentService = studentService;
		this.examRegistrationValidator = examRegistrationValidator;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(examRegistrationValidator);
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@GetMapping
	public ModelAndView getAllExamRegistrations(Model model) {
		return new ModelAndView("exam_registrations/list");
	}
	
	@GetMapping(value = "/register")
	public ModelAndView registerExam(Model model) {
		return new ModelAndView("exam_registrations/register");
	}
	
	@PostMapping
	public String add(@ModelAttribute @Validated ExamRegistrationDto examRegistrationDto, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("invalid", "One or more fields are invalid");
			return "exam_registrations/register";
	    }
		
		ExamRegistration examRegistration = new ExamRegistration();
		examRegistration.setDate(LocalDate.now());
		
		Exam exam = this.examService.findById(examRegistrationDto.getExamId());
		examRegistration.setExam(exam);
		
		Student student = this.studentService.findById(examRegistrationDto.getStudentId());
		examRegistration.setStudent(student);
		
		try {
			this.examRegistrationService.save(examRegistration);
			model.addAttribute("successAlertMessage", "Exam registration is created successfully");
			return "exam_registrations/register";
		} catch (Exception e) {
			model.addAttribute("invalid", "Exam registration has failed, try again later");
			return "exam_registrations/register";
		}
	}
	
	@ModelAttribute(name = "examRegistrationDto")
	private ExamRegistrationDto createExamRegistrationDto() {
		return new ExamRegistrationDto();
	}
	
	@ModelAttribute(name = "exams")
	private Map<Long, String> getExams() {
		return this.examService
				.findAllActive()
				.stream()
				.collect(Collectors.toMap(Exam::getId, 
						exam -> exam.getDate().toString() + " - " + exam.getSubject().getName()
						+ " - " + exam.getProfessor().getFirstName() + " " + exam.getProfessor().getLastName()));
	}
	
	@ModelAttribute(name = "students")
	private Map<Long, String> getStudents() {
		return this.studentService
				.findAll()
				.stream()
				.collect(Collectors.toMap(Student::getId, 
						student -> student.getIndexNumber() + " - " + student.getFirstName() + " " + student.getLastName() 
						+ " - year: " + student.getCurrentYearOfStudy()));
	}
}
