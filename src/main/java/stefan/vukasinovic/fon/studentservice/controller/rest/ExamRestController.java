package stefan.vukasinovic.fon.studentservice.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.model.EntityListDto;
import stefan.vukasinovic.fon.studentservice.service.ExamService;

@RestController
@RequestMapping("/rest/exams")
public class ExamRestController {

	private ExamService examService;
	
	@Autowired
	public ExamRestController(ExamService examService) {
		this.examService = examService;
	}
	
	@GetMapping
	public ResponseEntity<EntityListDto<Exam>> getExams(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
		int page = (offset / limit);
		System.out.println("Retrieving exams page " + page + " with size " + limit);
		List<Exam> exams = this.examService.findAll(page, limit).getContent();
		System.out.println("Number of exams retrieved: " + exams.size());
		return ResponseEntity.ok(new EntityListDto<Exam>(this.examService.count(), exams));
	}
}