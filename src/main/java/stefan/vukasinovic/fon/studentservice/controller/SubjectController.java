package stefan.vukasinovic.fon.studentservice.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import stefan.vukasinovic.fon.studentservice.entity.SemesterType;
import stefan.vukasinovic.fon.studentservice.entity.Subject;
import stefan.vukasinovic.fon.studentservice.service.SubjectService;
import stefan.vukasinovic.fon.studentservice.validator.SubjectValidator;

@Controller
@RequestMapping(value = "/subjects")
public class SubjectController {

	private SubjectService subjectService;
	private SubjectValidator subjectValidator;
	
	@Autowired
	public SubjectController(SubjectService subjectService, SubjectValidator subjectValidator) {
		super();
		this.subjectService = subjectService;
		this.subjectValidator = subjectValidator;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(subjectValidator);
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	@GetMapping
	public ModelAndView getAllSubjects(Model model) {
		return new ModelAndView("subjects/list");
	}
	
	@GetMapping(value = "/add")
	public ModelAndView addSubject(Model model) {
		return new ModelAndView("subjects/addOrUpdate");
	}
	
	@GetMapping(value = "/{id}/update")
	public ModelAndView updateSubject(@PathVariable Long id, Model model) {
		Subject subject = this.subjectService.findById(id);
		
		if (subject != null) {
			model.addAttribute("subject", subject);
		}
		
		return new ModelAndView("subjects/addOrUpdate");
	}
	
	@PostMapping
	public String addOrUpdate(@ModelAttribute @Validated Subject subject, BindingResult result, Model model) {
		
		if (result.hasErrors()) {
			model.addAttribute("invalid", "One or more fields are invalid");
			return "subjects/addOrUpdate";
	    }
		
		try {
			if (subject.getId() == null) {
				this.subjectService.save(subject);
				model.addAttribute("successAlertMessage", "Subject " + subject.getName() + " is added successfully");
			} else if (this.subjectService.findById(subject.getId()) != null) {
				this.subjectService.save(subject);
				model.addAttribute("successAlertMessage", "Data about subject is updated successfully");
			} else {
				model.addAttribute("invalid", "Subject with id " + subject.getId() + " does not exists");
			}
		
			return "subjects/addOrUpdate";
		} catch (Exception e) {
			model.addAttribute("invalid", "Something went wrong, try again later");
			return "subjects/addOrUpdate";
		}
	}
	
	@ModelAttribute(name = "subject")
	private Subject createSubject() {
		return new Subject();
	}
	
	@ModelAttribute(name = "semesters")
	private Map<String, String> getSemesters() {
		return Arrays.stream(SemesterType.values()).collect(Collectors.toMap(SemesterType::name, SemesterType::toString));
	}
}
