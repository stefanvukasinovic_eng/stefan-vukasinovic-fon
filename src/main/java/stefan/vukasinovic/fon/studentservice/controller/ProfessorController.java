package stefan.vukasinovic.fon.studentservice.controller;

import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import stefan.vukasinovic.fon.studentservice.entity.City;
import stefan.vukasinovic.fon.studentservice.entity.Professor;
import stefan.vukasinovic.fon.studentservice.entity.Title;
import stefan.vukasinovic.fon.studentservice.model.ProfessorDto;
import stefan.vukasinovic.fon.studentservice.service.CityService;
import stefan.vukasinovic.fon.studentservice.service.ProfessorService;
import stefan.vukasinovic.fon.studentservice.service.TitleService;
import stefan.vukasinovic.fon.studentservice.validator.ProfessorValidator;

@Controller
@RequestMapping(value = "/professors")
public class ProfessorController {

	private ProfessorService professorService;
	private CityService cityService;
	private TitleService titleService;
	private ProfessorValidator professorValidator;
	private ModelMapper modelMapper;
	
	@Autowired
	public ProfessorController(ProfessorService professorService, CityService cityService, TitleService titleService,
			ProfessorValidator professorValidator, ModelMapper modelMapper) {
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
		this.professorValidator = professorValidator;
		this.modelMapper = modelMapper;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(professorValidator);
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	@GetMapping
	public ModelAndView getAllProfessors(Model model) {
		return new ModelAndView("professors/list");
	}
	
	@GetMapping(value = "/add")
	public ModelAndView addProfessor(Model model) {
		return new ModelAndView("professors/addOrUpdate");
	}
	
	@GetMapping(value = "/{id}/update")
	public ModelAndView updateProfessor(@PathVariable Long id, Model model) {
		Professor professor = this.professorService.findById(id);
		
		ProfessorDto professorDto;
		if (professor != null) {
			professorDto = this.modelMapper.map(professor, ProfessorDto.class);
			professorDto.setCityId(professor.getCity().getId());
			professorDto.setTitleId(professor.getTitle().getId());
			professorDto.setReelectionDate(professor.getReelectionDate().toString());
		} else {
			professorDto = new ProfessorDto();
		}
		model.addAttribute("professorDto", professorDto);
		return new ModelAndView("professors/addOrUpdate");
	}
	
	@PostMapping
	public String addOrUpdate(@ModelAttribute @Validated ProfessorDto professorDto, BindingResult result, Model model) {
		
		if (result.hasErrors()) {
			model.addAttribute("invalid", "One or more fields are invalid");
			return "professors/addOrUpdate";
	    }
		
		Professor professor = this.modelMapper.map(professorDto, Professor.class);
		professor.setReelectionDate(LocalDate.parse(professorDto.getReelectionDate()));
		City city = this.cityService.findById(professorDto.getCityId());
		if (city != null) {
			professor.setCity(city);			
		}
		
		try {
			if (professor.getId() == null) {
				this.professorService.save(professor);
				model.addAttribute("successAlertMessage", "Professor " + professor.getFirstName() + " " + professor.getLastName() + " is added successfully");
			} else if (this.professorService.findById(professor.getId()) != null) {
				this.professorService.save(professor);
				model.addAttribute("successAlertMessage", "Data about professor is updated successfully");
			} else {
				model.addAttribute("invalid", "Professor with id " + professor.getId() + " does not exists");
			}
		
			return "professors/addOrUpdate";
		} catch (Exception e) {
			model.addAttribute("invalid", "Professor with email '" + professor.getEmail() + "' already exists");
			return "professors/addOrUpdate";
		}
	}
	
	@ModelAttribute(name = "professorDto")
	private ProfessorDto createProfessorDto() {
		return new ProfessorDto();
	}
	
	@ModelAttribute(name = "cities")
	private Map<Long, String> getCities() {
		return this.cityService
				.findAll()
				.stream()
				.collect(Collectors.toMap(City::getId, city -> city.getPostalCode() + " " + city.getName()));
	}
	
	@ModelAttribute(name = "titles")
	private Map<Long, String> getTitles() {
		return this.titleService
				.findAll()
				.stream()
				.collect(Collectors.toMap(Title::getId, Title::getName));
	}	
}