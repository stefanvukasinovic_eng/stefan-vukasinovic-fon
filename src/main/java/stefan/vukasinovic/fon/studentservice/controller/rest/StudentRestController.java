package stefan.vukasinovic.fon.studentservice.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import stefan.vukasinovic.fon.studentservice.entity.Student;
import stefan.vukasinovic.fon.studentservice.model.ResponseDto;
import stefan.vukasinovic.fon.studentservice.model.EntityListDto;
import stefan.vukasinovic.fon.studentservice.service.StudentService;

@RestController
@RequestMapping("/rest/students")
public class StudentRestController {

	private StudentService studentService;
	
	@Autowired
	public StudentRestController(StudentService studentService) {
		this.studentService = studentService;
	}
	
	@GetMapping
	public ResponseEntity<EntityListDto<Student>> getStudents(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
		int page = (offset / limit);
		System.out.println("Retrieving students page " + page + " with size " + limit);
		List<Student> students = this.studentService.findAll(page, limit).getContent();
		System.out.println("Number of students retrieved: " + students.size());
		return ResponseEntity.ok(new EntityListDto<Student>(this.studentService.count(), students));
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		System.out.println("Deleting student with id " + id);
		try {
			this.studentService.delete(id);
			return ResponseEntity.ok(new ResponseDto("You have deleted student with id " + id + " successfully", true));
		} catch(Exception e) {
			return ResponseEntity.badRequest().body("Student with id " + id + " is not deleted successfully");
		}
	}
}