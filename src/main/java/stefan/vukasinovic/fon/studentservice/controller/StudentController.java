package stefan.vukasinovic.fon.studentservice.controller;

import java.util.Map;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import stefan.vukasinovic.fon.studentservice.entity.City;
import stefan.vukasinovic.fon.studentservice.entity.Student;
import stefan.vukasinovic.fon.studentservice.model.StudentDto;
import stefan.vukasinovic.fon.studentservice.service.CityService;
import stefan.vukasinovic.fon.studentservice.service.StudentService;
import stefan.vukasinovic.fon.studentservice.validator.StudentValidator;

@Controller
@RequestMapping(value = "/students")
public class StudentController {

	private StudentService studentService;
	private CityService cityService;
	private StudentValidator studentValidator;
	private ModelMapper modelMapper;
	
	@Autowired
	public StudentController(StudentService studentService, CityService cityService, StudentValidator studentValidator, ModelMapper modelMapper) {
		this.studentService = studentService;
		this.cityService = cityService;
		this.studentValidator = studentValidator;
		this.modelMapper = modelMapper;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(studentValidator);
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@GetMapping
	public ModelAndView getAllStudents(Model model) {
		return new ModelAndView("students/list");
	}
	
	@GetMapping(value = "/add")
	public ModelAndView addStudent(Model model) {
		return new ModelAndView("students/addOrUpdate");
	}
	
	@GetMapping(value = "/{id}/update")
	public ModelAndView updateStudent(@PathVariable Long id, Model model) {
		Student student = this.studentService.findById(id);
		
		StudentDto studentDto;
		if (student != null) {
			studentDto = this.modelMapper.map(student, StudentDto.class);
			studentDto.setCityId(student.getCity().getId());
		} else {
			studentDto = new StudentDto();
		}
		
		model.addAttribute("studentDto", studentDto);
		return new ModelAndView("students/addOrUpdate");
	}
	
	@PostMapping
	public String addOrUpdate(@ModelAttribute @Validated StudentDto studentDto, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("invalid", "One or more fields are invalid");
			return "students/addOrUpdate";
	    }
		
		Student student = this.modelMapper.map(studentDto, Student.class);
		City city = this.cityService.findById(studentDto.getCityId());
		if (city != null) {
			student.setCity(city);			
		}
		
		try {
			//If there is no id, then we perform save else if user with given id exists, then we perform update
			//Third case is where id is not null, but student with that id doesn't exists which we consider as an error
			if (student.getId() == null) {
				this.studentService.save(student);
				model.addAttribute("successAlertMessage", "Student with index number " + student.getIndexNumber() + " is added successfully");
			} else if (this.studentService.findById(student.getId()) != null) {
				this.studentService.save(student);
				model.addAttribute("successAlertMessage", "Data about student is updated successfully");
			} else {
				model.addAttribute("invalid", "Student with id " + student.getId() + " does not exists");
			}
		
			return "students/addOrUpdate";
		} catch (Exception e) {
			model.addAttribute("invalid", "Student with index number '" + student.getIndexNumber() + "' and/or email '" + student.getEmail() + "' already exists");
			return "students/addOrUpdate";
		}
	}
	
	@ModelAttribute(name = "studentDto")
	private StudentDto createStudentDto() {
		return new StudentDto();
	}
	
	@ModelAttribute(name = "cities")
	private Map<Long, String> getCities() {
		return this.cityService
				.findAll()
				.stream()
				.collect(Collectors.toMap(City::getId, city -> city.getPostalCode() + " " + city.getName()));
	}
}
