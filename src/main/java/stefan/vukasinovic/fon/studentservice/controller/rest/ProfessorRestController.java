package stefan.vukasinovic.fon.studentservice.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import stefan.vukasinovic.fon.studentservice.entity.Professor;
import stefan.vukasinovic.fon.studentservice.model.EntityListDto;
import stefan.vukasinovic.fon.studentservice.model.ResponseDto;
import stefan.vukasinovic.fon.studentservice.service.ProfessorService;

@RestController
@RequestMapping("/rest/professors")
public class ProfessorRestController {

	private ProfessorService professorService;
	
	@Autowired
	public ProfessorRestController(ProfessorService professorService) {
		this.professorService = professorService;
	}
	
	@GetMapping
	public ResponseEntity<EntityListDto<Professor>> getProfessors(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
		int page = (offset / limit);
		System.out.println("Retrieving professors page " + page + " with size " + limit);
		List<Professor> professors = this.professorService.findAll(page, limit).getContent();
		System.out.println("Number of professors retrieved: " + professors.size());
		return ResponseEntity.ok(new EntityListDto<Professor>(this.professorService.count(), professors));
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		System.out.println("Deleting professor with id " + id);
		try {
			this.professorService.delete(id);
			return ResponseEntity.ok(new ResponseDto("You have deleted professor with id " + id + " successfully", true));
		} catch(Exception e) {
			return ResponseEntity.badRequest().body("Professor with id " + id + " is not deleted successfully");
		}
	}
}