package stefan.vukasinovic.fon.studentservice;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import stefan.vukasinovic.fon.studentservice.config.StudentServiceDatabaseConfig;
import stefan.vukasinovic.fon.studentservice.config.StudentServiceWebMvcConfig;
import stefan.vukasinovic.fon.studentservice.security.WebSecurityConfig;

public class StudentServiceAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[]{StudentServiceDatabaseConfig.class, WebSecurityConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[]{StudentServiceWebMvcConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{"/"};
	}
}