package stefan.vukasinovic.fon.studentservice.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import stefan.vukasinovic.fon.studentservice.entity.Subject;

@Component
public class SubjectValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Subject.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Subject subject = (Subject) target;
		
		System.out.println("Validating subject: " + subject.getName());
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.subject.name");
		
		if (errors.hasErrors()) {
			return;
		}
		
		if (subject.getName().length() < 3 || subject.getName().length() > 30) {
			errors.rejectValue("name", "Valid.subject.name");
		}
		
		if (subject.getDescription() != null && subject.getDescription().length() > 200) {
			errors.rejectValue("description", "Valid.subject.description");
		}
		
		if (subject.getYearOfStudy() != null && (subject.getYearOfStudy() < 1 || subject.getYearOfStudy() > 9)) {
			errors.rejectValue("yearOfStudy", "Valid.subject.yearOfStudy");
		}
	}	
}