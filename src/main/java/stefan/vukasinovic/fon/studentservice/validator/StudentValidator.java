package stefan.vukasinovic.fon.studentservice.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import stefan.vukasinovic.fon.studentservice.model.StudentDto;

@Component
public class StudentValidator implements Validator {
	
	private EmailValidator emailValidator;
	
	@Autowired
	public StudentValidator(EmailValidator emailValidator) {
		this.emailValidator = emailValidator;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return StudentDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		StudentDto studentDto = (StudentDto) target;
		
		System.out.println("Validating student with index number: " + studentDto.getIndexNumber());
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "indexNumber", "NotEmpty.student.indexNumber");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.entity.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.entity.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "currentYearOfStudy", "NotEmpty.student.currentYearOfStudy");
		
		if (errors.hasErrors()) {
			return;
		}
		
		if (studentDto.getIndexNumber().length() != 10) {
			errors.rejectValue("indexNumber", "Valid.student.indexNumber");
		}
		
		if (studentDto.getFirstName().length() < 3 || studentDto.getFirstName().length() > 30) {
			errors.rejectValue("firstName", "Valid.entity.firstName");
		}
		
		if (studentDto.getLastName().length() < 3 || studentDto.getLastName().length() > 30) {
			errors.rejectValue("lastName", "Valid.entity.lastName");
		}
		
		if (studentDto.getEmail() != null && studentDto.getEmail().length() > 30) {
			errors.rejectValue("email", "Valid.entity.email");
		}
		
		if (studentDto.getEmail() != null && !emailValidator.valid(studentDto.getEmail())) {
			errors.rejectValue("email", "Pattern.entity.email");
		}
		
		if (studentDto.getAddress() != null && (studentDto.getAddress().length() < 3 || studentDto.getAddress().length() > 50)) {
			errors.rejectValue("address", "Valid.entity.address");
		}
		
		if (studentDto.getPhone() != null && (studentDto.getPhone().length() < 6 || studentDto.getPhone().length() > 15)) {
			errors.rejectValue("phone", "Valid.entity.phone");
		}
		
		if (studentDto.getCurrentYearOfStudy() <= 0) {
			errors.rejectValue("currentYearOfStudy", "Valid.student.currentYearOfStudy");
		}
	}	
}