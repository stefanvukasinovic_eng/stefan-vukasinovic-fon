package stefan.vukasinovic.fon.studentservice.validator;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.model.ExamDto;
import stefan.vukasinovic.fon.studentservice.service.ExamService;

@Component
public class ExamValidator implements Validator {
	
	private ExamService examService;
	
	@Autowired
	public ExamValidator(ExamService examService) {
		this.examService = examService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return ExamDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ExamDto examDto = (ExamDto) target;
		
		System.out.println("Exam validation");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "NotEmpty.exam.date");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subjectId", "NotEmpty.exam.subject");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "professorId", "NotEmpty.exam.professor");
		
		if (errors.hasErrors()) {
			return;
		}
		
		LocalDate date;
		try {
			date = LocalDate.parse(examDto.getDate());
		} catch (Exception e) {
			errors.rejectValue("date", "Format.exam.date");
			return;
		}
		
		if (date.isBefore(LocalDate.now().plusDays(1))) {
			errors.rejectValue("date", "Valid.exam.date");
		}
		
		List<Exam> examsBySubject = this.examService.findAllActiveBySubject(examDto.getSubjectId(), LocalDate.now());
		if (!examsBySubject.isEmpty()) {
			errors.rejectValue("subjectId", "Valid.exam.subject");
		}
	}	
}