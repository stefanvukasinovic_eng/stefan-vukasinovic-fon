package stefan.vukasinovic.fon.studentservice.validator;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.entity.ExamRegistration;
import stefan.vukasinovic.fon.studentservice.entity.Student;
import stefan.vukasinovic.fon.studentservice.model.ExamRegistrationDto;
import stefan.vukasinovic.fon.studentservice.service.ExamRegistrationService;
import stefan.vukasinovic.fon.studentservice.service.ExamService;
import stefan.vukasinovic.fon.studentservice.service.StudentService;

@Component
public class ExamRegistrationValidator implements Validator {
	
	private ExamRegistrationService examRegistrationService;
	private ExamService examService;
	private StudentService studentService;
	
	@Autowired
	public ExamRegistrationValidator(ExamRegistrationService examRegistrationService, ExamService examService,
			StudentService studentService) {
		this.examRegistrationService = examRegistrationService;
		this.examService = examService;
		this.studentService = studentService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return ExamRegistrationDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ExamRegistrationDto examRegistrationDto = (ExamRegistrationDto) target;
		
		System.out.println("Exam registration validation");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "examId", "NotEmpty.examRegistration.exam");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "studentId", "NotEmpty.examRegistration.student");
		
		if (errors.hasErrors()) {
			return;
		}
		

		//Check if registration is in the last week before exam
		Exam exam = this.examService.findById(examRegistrationDto.getExamId());
		if (exam.getDate().isAfter(LocalDate.now().plusDays(7))) {
			errors.rejectValue("examId", "Valid.examRegistration.exam");
		}
		
		//Check if student is eligible to take an exam
		Student student = this.studentService.findById(examRegistrationDto.getStudentId());
		if (exam.getSubject().getYearOfStudy() != null && student.getCurrentYearOfStudy() < exam.getSubject().getYearOfStudy()) {
			errors.rejectValue("studentId", "Valid.examRegistration.student");
		}
		
		//Check if student is already registered for chosen exam
		List<ExamRegistration> examRegistrations = this.examRegistrationService.findAllByExamAndStudent(exam, student);
		if (!examRegistrations.isEmpty()) {
			errors.rejectValue("studentId", "Exists.examRegistration.student");
		}
	}	
}