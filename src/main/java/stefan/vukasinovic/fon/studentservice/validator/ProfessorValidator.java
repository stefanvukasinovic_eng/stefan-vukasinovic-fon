package stefan.vukasinovic.fon.studentservice.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import stefan.vukasinovic.fon.studentservice.model.ProfessorDto;

@Component
public class ProfessorValidator implements Validator {
	
	private EmailValidator emailValidator;
	
	@Autowired
	public ProfessorValidator(EmailValidator emailValidator) {
		this.emailValidator = emailValidator;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return ProfessorDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ProfessorDto professorDto = (ProfessorDto) target;
		
		System.out.println("Validating professor: " + professorDto.getFirstName() + " " + professorDto.getLastName());
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.entity.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.entity.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "reelectionDate", "NotEmpty.professor.reelectionDate");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "titleId", "NotEmpty.professor.titleId");
		
		if (errors.hasErrors()) {
			return;
		}
		
		if (professorDto.getFirstName().length() < 3 || professorDto.getFirstName().length() > 30) {
			errors.rejectValue("firstName", "Valid.entity.firstName");
		}
		
		if (professorDto.getLastName().length() < 3 || professorDto.getLastName().length() > 30) {
			errors.rejectValue("lastName", "Valid.entity.lastName");
		}
		
		if (professorDto.getEmail() != null && professorDto.getEmail().length() > 30) {
			errors.rejectValue("email", "Valid.entity.email");
		}
		
		if (professorDto.getEmail() != null && !emailValidator.valid(professorDto.getEmail())) {
			errors.rejectValue("email", "Pattern.entity.email");
		}

		if (professorDto.getAddress() != null && (professorDto.getAddress().length() < 3 || professorDto.getAddress().length() > 50)) {
			errors.rejectValue("address", "Valid.entity.address");
		}
		
		if (professorDto.getPhone() != null && (professorDto.getPhone().length() < 6 || professorDto.getPhone().length() > 15)) {
			errors.rejectValue("phone", "Valid.entity.phone");
		}
		
		if (professorDto.getTitleId() <= 0) {
			errors.rejectValue("titleId", "Valid.professor.titleId");
		}
	}	
}