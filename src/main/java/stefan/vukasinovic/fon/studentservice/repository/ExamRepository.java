package stefan.vukasinovic.fon.studentservice.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import stefan.vukasinovic.fon.studentservice.entity.Exam;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long> { 
	
	List<Exam> findAllByDateGreaterThan(LocalDate date);
	
	List<Exam> findAllBySubject_IdAndDateGreaterThan(Long subjectId, LocalDate date);
}