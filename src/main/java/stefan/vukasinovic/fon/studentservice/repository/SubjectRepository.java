package stefan.vukasinovic.fon.studentservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import stefan.vukasinovic.fon.studentservice.entity.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> { }