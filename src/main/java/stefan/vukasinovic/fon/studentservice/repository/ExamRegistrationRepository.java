package stefan.vukasinovic.fon.studentservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.entity.ExamRegistration;
import stefan.vukasinovic.fon.studentservice.entity.Student;

@Repository
public interface ExamRegistrationRepository extends JpaRepository<ExamRegistration, Long> {
	
	List<ExamRegistration> findAllByExamAndStudent(Exam exam, Student student);
}