package stefan.vukasinovic.fon.studentservice.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import stefan.vukasinovic.fon.studentservice.entity.Student;
import stefan.vukasinovic.fon.studentservice.repository.StudentRepository;
import stefan.vukasinovic.fon.studentservice.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {
	
	private StudentRepository studentRepository;
	
	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository) {
		this.studentRepository = studentRepository;
	}

	@Override
	public long count() {
		return this.studentRepository.count();
	}
	
	@Override
	public Student findById(Long id) {
		return this.studentRepository.findById(id).orElse(null);
	}
	
	@Override
	public Page<Student> findAll(int page, int size) {
		return this.studentRepository.findAll(PageRequest.of(page,  size));
	}
	
	@Override
	public List<Student> findAll() {
		return this.studentRepository.findAll();
	}
	
	@Override
	public void save(Student student) {
		this.studentRepository.save(student);
	}

	@Override
	public void delete(Long id) {
		this.studentRepository.deleteById(id);
	}
}