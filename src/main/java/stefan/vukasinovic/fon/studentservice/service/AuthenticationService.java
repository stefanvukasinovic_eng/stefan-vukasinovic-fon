package stefan.vukasinovic.fon.studentservice.service;

import org.springframework.security.core.AuthenticationException;

import stefan.vukasinovic.fon.studentservice.model.AuthenticationRequest;

public interface AuthenticationService {
	
	void authenticate(AuthenticationRequest authenticationRequest) throws AuthenticationException;
}