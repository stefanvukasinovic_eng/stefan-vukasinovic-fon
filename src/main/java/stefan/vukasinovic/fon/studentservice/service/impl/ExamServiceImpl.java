package stefan.vukasinovic.fon.studentservice.service.impl;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.repository.ExamRepository;
import stefan.vukasinovic.fon.studentservice.service.ExamService;

@Service
@Transactional
public class ExamServiceImpl implements ExamService {

	private ExamRepository examRepository;

	@Autowired
	public ExamServiceImpl(ExamRepository examRepository) {
		super();
		this.examRepository = examRepository;
	}
	
	@Override
	public long count() {
		return this.examRepository.count();
	}

	@Override
	public Exam findById(Long id) {
		return this.examRepository.findById(id).orElse(null);
	}

	@Override
	public Page<Exam> findAll(int page, int size) {
		return this.examRepository.findAll(PageRequest.of(page, size));
	}
	
	@Override
	public List<Exam> findAllActive() {
		return this.examRepository.findAllByDateGreaterThan(LocalDate.now());
	}

	@Override
	public void save(Exam exam) {
		this.examRepository.save(exam);
	}

	@Override
	public List<Exam> findAllActiveBySubject(Long subjectId, LocalDate date) {
		return this.examRepository.findAllBySubject_IdAndDateGreaterThan(subjectId, date);
	}
}