package stefan.vukasinovic.fon.studentservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.entity.ExamRegistration;
import stefan.vukasinovic.fon.studentservice.entity.Student;

public interface ExamRegistrationService {
	
	long count();
	
	ExamRegistration findById(Long id);
	
	Page<ExamRegistration> findAll(int page, int size);
	
	List<ExamRegistration> findAllByExamAndStudent(Exam exam, Student student);
	
	void save(ExamRegistration examRegistration);
}