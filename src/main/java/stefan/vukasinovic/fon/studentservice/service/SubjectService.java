package stefan.vukasinovic.fon.studentservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import stefan.vukasinovic.fon.studentservice.entity.Subject;

public interface SubjectService {
	
	long count();
	
	Subject findById(Long id);
	
	Page<Subject> findAll(int page, int size);
	
	List<Subject> findAll();
	
	void save(Subject subject);
	
	void delete(Long id);
}