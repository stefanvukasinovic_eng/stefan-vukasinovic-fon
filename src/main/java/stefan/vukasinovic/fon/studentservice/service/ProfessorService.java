package stefan.vukasinovic.fon.studentservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import stefan.vukasinovic.fon.studentservice.entity.Professor;

public interface ProfessorService {
	
	long count();
	
	Professor findById(Long id);
	
	Page<Professor> findAll(int page, int size);
	
	List<Professor> findAll();
	
	void save(Professor professor);
	
	void delete(Long id);
}