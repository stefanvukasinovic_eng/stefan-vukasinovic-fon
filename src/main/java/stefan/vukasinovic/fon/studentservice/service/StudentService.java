package stefan.vukasinovic.fon.studentservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import stefan.vukasinovic.fon.studentservice.entity.Student;

public interface StudentService {
	
	long count();
	
	Student findById(Long id);
	
	Page<Student> findAll(int page, int size);
	
	List<Student> findAll();
	
	void save(Student student);
	
	void delete(Long id);
}