package stefan.vukasinovic.fon.studentservice.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import stefan.vukasinovic.fon.studentservice.entity.Professor;
import stefan.vukasinovic.fon.studentservice.repository.ProfessorRepository;
import stefan.vukasinovic.fon.studentservice.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService {

	private ProfessorRepository professorRepository;
	
	@Autowired
	public ProfessorServiceImpl(ProfessorRepository professorRepository) {
		this.professorRepository = professorRepository;
	}

	@Override
	public long count() {
		return this.professorRepository.count();
	}

	@Override
	public Professor findById(Long id) {
		return this.professorRepository.findById(id).orElse(null);
	}

	@Override
	public Page<Professor> findAll(int page, int size) {
		return this.professorRepository.findAll(PageRequest.of(page,  size));
	}

	@Override
	public List<Professor> findAll() {
		return this.professorRepository.findAll();
	}
	
	@Override
	public void save(Professor professor) {
		this.professorRepository.save(professor);
	}

	@Override
	public void delete(Long id) {
		this.professorRepository.deleteById(id);
	}
}