package stefan.vukasinovic.fon.studentservice.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.vukasinovic.fon.studentservice.entity.Title;
import stefan.vukasinovic.fon.studentservice.repository.TitleRepository;
import stefan.vukasinovic.fon.studentservice.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService {

	private TitleRepository titleRepository;
	
	@Autowired
	public TitleServiceImpl(TitleRepository titleRepository) {
		this.titleRepository = titleRepository;
	}

	@Override
	public Title findById(Long id) {
		return this.titleRepository.findById(id).orElse(null);
	}

	@Override
	public List<Title> findAll() {
		return this.titleRepository.findAll();
	}
}