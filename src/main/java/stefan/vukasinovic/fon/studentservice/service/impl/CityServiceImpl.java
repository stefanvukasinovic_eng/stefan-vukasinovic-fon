package stefan.vukasinovic.fon.studentservice.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import stefan.vukasinovic.fon.studentservice.entity.City;
import stefan.vukasinovic.fon.studentservice.repository.CityRepository;
import stefan.vukasinovic.fon.studentservice.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {
	
	private CityRepository cityRepository;
	
	@Autowired
	public CityServiceImpl(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}
	
	@Override
	public City findById(Long id) {
		return this.cityRepository.findById(id).orElse(null);
	}
	
	@Override
	public List<City> findAll() {
		return this.cityRepository.findAll();
	}
}