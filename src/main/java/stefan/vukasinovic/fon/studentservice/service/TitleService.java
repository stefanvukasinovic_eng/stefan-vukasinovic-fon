package stefan.vukasinovic.fon.studentservice.service;

import java.util.List;

import stefan.vukasinovic.fon.studentservice.entity.Title;

public interface TitleService {
	
	Title findById(Long id);
	
	List<Title> findAll();
}