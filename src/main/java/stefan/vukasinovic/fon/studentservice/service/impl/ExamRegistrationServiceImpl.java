package stefan.vukasinovic.fon.studentservice.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import stefan.vukasinovic.fon.studentservice.entity.Exam;
import stefan.vukasinovic.fon.studentservice.entity.ExamRegistration;
import stefan.vukasinovic.fon.studentservice.entity.Student;
import stefan.vukasinovic.fon.studentservice.repository.ExamRegistrationRepository;
import stefan.vukasinovic.fon.studentservice.service.ExamRegistrationService;

@Service
@Transactional
public class ExamRegistrationServiceImpl implements ExamRegistrationService {

	private ExamRegistrationRepository examRegistrationRepository;
	
	@Autowired
	public ExamRegistrationServiceImpl(ExamRegistrationRepository examRegistrationRepository) {
		this.examRegistrationRepository = examRegistrationRepository;
	}

	@Override
	public long count() {
		return this.examRegistrationRepository.count();
	}

	@Override
	public ExamRegistration findById(Long id) {
		return this.examRegistrationRepository.findById(id).orElse(null);
	}

	@Override
	public Page<ExamRegistration> findAll(int page, int size) {
		return this.examRegistrationRepository.findAll(PageRequest.of(page, size));
	}
	
	@Override
	public List<ExamRegistration> findAllByExamAndStudent(Exam exam, Student student) {
		return this.examRegistrationRepository.findAllByExamAndStudent(exam, student);
	}

	@Override
	public void save(ExamRegistration examRegistration) {
		this.examRegistrationRepository.save(examRegistration);
	}
}