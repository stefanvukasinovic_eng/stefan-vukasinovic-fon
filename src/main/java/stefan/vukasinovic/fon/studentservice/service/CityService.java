package stefan.vukasinovic.fon.studentservice.service;

import java.util.List;

import stefan.vukasinovic.fon.studentservice.entity.City;

public interface CityService {
	
	City findById(Long id);
	
	List<City> findAll();
}