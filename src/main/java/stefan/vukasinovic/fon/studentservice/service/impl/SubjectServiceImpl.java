package stefan.vukasinovic.fon.studentservice.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import stefan.vukasinovic.fon.studentservice.entity.Subject;
import stefan.vukasinovic.fon.studentservice.repository.SubjectRepository;
import stefan.vukasinovic.fon.studentservice.service.SubjectService;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {

	private SubjectRepository subjectRepository;
	
	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository) {
		this.subjectRepository = subjectRepository;
	}

	@Override
	public long count() {
		return this.subjectRepository.count();
	}

	@Override
	public Subject findById(Long id) {
		return this.subjectRepository.findById(id).orElse(null);
	}

	@Override
	public Page<Subject> findAll(int page, int size) {
		return this.subjectRepository.findAll(PageRequest.of(page, size));
	}
	
	@Override
	public List<Subject> findAll() {
		return this.subjectRepository.findAll();
	}

	@Override
	public void save(Subject subject) {
		this.subjectRepository.save(subject);
	}

	@Override
	public void delete(Long id) {
		this.subjectRepository.deleteById(id);
	}
}