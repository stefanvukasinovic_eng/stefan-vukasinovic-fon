package stefan.vukasinovic.fon.studentservice.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService { }