package stefan.vukasinovic.fon.studentservice.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;

import stefan.vukasinovic.fon.studentservice.entity.Exam;

public interface ExamService {
	
	long count();
	
	Exam findById(Long id);
	
	Page<Exam> findAll(int page, int size);
	
	List<Exam> findAllActive();
	
	void save(Exam exam);
	
	List<Exam> findAllActiveBySubject(Long subjectId, LocalDate date);
}