package stefan.vukasinovic.fon.studentservice.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

@Entity
public class Subject {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	@Length(min = 3, max = 30)
	private String name;
	
	@Column(length = 200)
	private String description;
	
	@Column
	@Range(min = 1, max = 9)
	private Long yearOfStudy;
	
	@Column
	@Enumerated(EnumType.STRING)
	private SemesterType semester;

	public Subject() { }
	
	public Subject(@Length(min = 3, max = 30) String name, String description, @Range(min = 1, max = 9) Long yearOfStudy,
			SemesterType semester) {
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(Long yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public SemesterType getSemester() {
		return semester;
	}

	public void setSemester(SemesterType semester) {
		this.semester = semester;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subject other = (Subject) obj;
		return Objects.equals(id, other.id);
	}
}