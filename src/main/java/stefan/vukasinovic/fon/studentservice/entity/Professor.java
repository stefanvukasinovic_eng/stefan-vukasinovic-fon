package stefan.vukasinovic.fon.studentservice.entity;

import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.Length;

@Entity
public class Professor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	@Length(min = 3, max = 30)
	private String firstName;
	
	@Column(nullable = false)
	@Length(min = 3, max = 30)
	private String lastName;
	
	@Column(unique = true)
	@Length(min = 3, max = 30)
	private String email;
	
	@Column
	@Length(min = 3, max = 50)
	private String address;
	
	@ManyToOne
	@JoinColumn(name = "city_id", referencedColumnName = "id")
	private City city;
	
	@Column
	@Length(min = 6, max = 15)
	private String phone;
	
	@Column(nullable = false)
	private LocalDate reelectionDate;
	
	@ManyToOne
	@JoinColumn(name = "title_id", referencedColumnName = "id")
	private Title title;

	public Professor() { }

	public Professor(@Length(min = 3, max = 30) String firstName, @Length(min = 3, max = 30) String lastName,
			@Length(min = 3, max = 30) String email, @Length(min = 3, max = 50) String address, City city,
			@Length(min = 6, max = 15) String phone, LocalDate reelectionDate, Title title) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.city = city;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDate getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(LocalDate reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		return Objects.equals(id, other.id);
	}
}