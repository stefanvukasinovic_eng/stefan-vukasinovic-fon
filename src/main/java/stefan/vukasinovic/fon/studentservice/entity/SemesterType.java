package stefan.vukasinovic.fon.studentservice.entity;

public enum SemesterType {
	WINTER, SUMMER;
	
	@Override
	public String toString() {
		return super.toString().substring(0, 1).toUpperCase() + super.toString().substring(1).toLowerCase();
	}
}