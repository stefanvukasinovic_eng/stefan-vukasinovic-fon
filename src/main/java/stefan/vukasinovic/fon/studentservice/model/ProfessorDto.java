package stefan.vukasinovic.fon.studentservice.model;

public class ProfessorDto {
	
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String address;
	private Long cityId;
	private String phone;
	private String reelectionDate;
	private Long titleId;

	public ProfessorDto() { }

	public ProfessorDto(Long id, String firstName, String lastName, String email, String address, Long cityId,
			String phone, String reelectionDate, Long titleId) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.cityId = cityId;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.titleId = titleId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(String reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public Long getTitleId() {
		return titleId;
	}

	public void setTitleId(Long titleId) {
		this.titleId = titleId;
	}
}