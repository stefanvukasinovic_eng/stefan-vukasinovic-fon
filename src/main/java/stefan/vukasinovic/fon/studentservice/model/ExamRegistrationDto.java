package stefan.vukasinovic.fon.studentservice.model;

public class ExamRegistrationDto {
	
	private Long id;
	private Long examId;
	private Long studentId;

	public ExamRegistrationDto() { }

	public ExamRegistrationDto(Long id, Long examId, Long studentId) {
		this.id = id;
		this.examId = examId;
		this.studentId = studentId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getExamId() {
		return examId;
	}

	public void setExamId(Long examId) {
		this.examId = examId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
}