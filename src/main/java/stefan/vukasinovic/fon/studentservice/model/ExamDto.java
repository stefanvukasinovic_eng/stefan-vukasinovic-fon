package stefan.vukasinovic.fon.studentservice.model;

import javax.persistence.Entity;

@Entity
public class ExamDto {

	private Long id;
	private Long subjectId;
	private Long professorId;
	private String date;
	
	public ExamDto() { }

	public ExamDto(Long id, Long subjectId, Long professorId, String date) {
		this.id = id;
		this.subjectId = subjectId;
		this.professorId = professorId;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}

	public Long getProfessorId() {
		return professorId;
	}

	public void setProfessorId(Long professorId) {
		this.professorId = professorId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}